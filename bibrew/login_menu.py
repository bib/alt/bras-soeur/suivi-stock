"""Login menu"""
import os
import json
from bibrew.base_menus import BaseMenu
from lib.rich.rich.console import CancelByUser


class LoginMenu(BaseMenu):
    def enter(self):
        super().enter()
        self.rich_name = "Informations de connexion au serveur"
        self.itf.layout["side"].visible = False

    def exit(self):
        self.itf.layout["side"].visible = True

    def update(self):
        credentials_path = os.path.expanduser("~/.config/bibrew/credentials")
        if not os.path.exists(os.path.dirname(credentials_path)):
            os.makedirs(os.path.dirname(credentials_path))
        try:
            with open(credentials_path) as f:
                credentials = json.load(f)
        except FileNotFoundError:
            with open(credentials_path, "w") as f:
                f.write("")
            return
        except json.JSONDecodeError:
            self.display()
            self.itf.layout["footer"].visible = True
            try:
                url = self.itf.ask_str("URL de la base de données")
                last_char = url[-1]
                if last_char != "/":
                    url = url + "/"
                    user_name = self.itf.ask_str("Nom d'utilisateur")
                    password = self.itf.ask_str("Mot de passe")
            except CancelByUser:
                exit(0)
            credentials = {"url": url,
                           "user_name": user_name,
                           "password": password}
            with open(credentials_path, "w") as f:
                json.dump(credentials, f)
        self.itf.url = credentials["url"]
        self.itf.sess.auth = (credentials["user_name"],
                              credentials["password"])
        self.parent.state_stack.pop(0)
        self.parent.push_state("main_menu")
        self.itf.print_wait_init()
        return
