"""Pure functions"""
from datetime import datetime

class WrongRecipeType(Exception):
    "Defines wrong recipe type exception"
    pass


def xstr(string):
    """Returns empty string if string is None"""
    if string is None:
        return ''
    return str(string)


def lovibond_to_ebc(value):
    ebc = ((1.3546*value)-0.76)*1.97
    return ebc


def dens_temp(dens, temp):
    if dens < 2:
        dens = dens*1000
    dens_cor = dens + 0.00352871*(temp-20)**2 + 0.225225*(temp-20)
    return dens_cor/1000


def calculate_alcohol(d_deb, d_fin, sucre_emb=0):
    alc_vv = (d_deb - d_fin)*1.05*100/(d_fin*0.789) + 0.065*sucre_emb
    return alc_vv


def dens_to_plato(dens):
    if dens > 2:
        dens = dens/1000
    plato = (
        (-1 * 616.868) +
        (1111.14 * dens) -
        (630.272 * dens**2) +
        (135.997 * dens**3)
    )
    return plato


def plato_to_dens(plato):
    dens = 1 + (plato / (258.6 - ((plato/258.2)*227.1)))
    return dens


def pitch_rate_calc(vol, dens, beer_type):
    if beer_type in ["ALE", "SAISON", "VOSSKVEIK"]:
        nb_cells = 0.5*10**6*vol*1000*dens_to_plato(dens)
    elif beer_type == "LAGER":
        nb_cells = 10**6*vol*1000*dens_to_plato(dens)
    else:
        raise WrongRecipeType
    return nb_cells


def yeast_quantity(pitch, yeast_form):
    """Calculates yeast quantity corresponding to
    number of cells depending on the yeast form"""
    if yeast_form == "DRY":
        qty = pitch/(10**13)
    elif yeast_form == "LIQUID":
        qty = pitch/10**11
    return round(qty, 3)


def extract_date(date_time):
    date = datetime.strptime(date_time, "%Y-%m-%dT%H:%M:%S").\
        strftime("%Y-%m-%d")
    return date
