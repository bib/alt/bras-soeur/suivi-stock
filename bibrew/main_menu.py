"""Main menu definition"""
import pandas as pd
import requests as req

from bibrew.panels import Body
from bibrew.base_menus import MultiMenu
from bibrew.pures import extract_date


class MainMenu(MultiMenu):
    def __init__(self, name, itf):
        super().__init__(name, itf)
        self.add_option("i", "stocks_menu", "Inventaire", "i")
        self.add_option("b", "rawmats_menu", "Base de données", "b")
        self.add_option("r", "recipes_menu", "Recettes", "r")
        self.add_option("h", "records_menu", "Historique", "h")
        self.add_option("c", "orders_menu", "Clients et commandes", "c")
        self.add_option("o", "tools_menu", "Outils", "o")

    def enter(self):
        self.archived_recipes = self.get_archived_recipes()
        self.records = self.get_records()
        self.conditionings = self.get_conditionings()
        super().enter()
        self.itf.layout["body_header"].visible = True

    def exit(self):
        super().exit()
        self.itf.layout["body_header"].visible = False

    def get_rawmat_df(self, rawmat):
        nested_json = self.itf.sess.get(self.itf.url +
                                        self.itf.rawmats_dict[rawmat][2]).\
                                        json()
        pandas_df = pd.json_normalize(nested_json,
                                      self.itf.rawmats_dict[rawmat][2],
                                      ["name", "id"])
        if not pandas_df.empty:
            if rawmat == "y":
                pandas_df = pandas_df[['id', 'name', 'form', 'quantity']]
            else:
                pandas_df = pandas_df[['id', 'name', 'quantity']]
        return pandas_df

    def get_rawmat_qty(self, rawmat):
        rawmat_df = self.get_rawmat_df(rawmat)
        if rawmat_df.empty:
            rawmat_qty = pd.Series(0, index=["quantity"])
        else:
            rawmat_qty = rawmat_df[["quantity"]].sum()
        return rawmat_qty

    def get_archived_recipes(self):
        archived_recipes_json = self.itf.sess.get(self.itf.url +
                                                  "record/recipes").json()
        archived_recipes = pd.DataFrame(archived_recipes_json).\
            drop(columns="volume", errors="ignore")
        if archived_recipes.empty:
            archived_recipes = pd.DataFrame(columns=["id", "name"])
        return archived_recipes

    def get_records(self):
        records_json = self.itf.sess.get(self.itf.url + "records").json()
        records_df = pd.DataFrame(records_json)
        return records_df

    def get_conditionings(self):
        conditionings = pd.DataFrame()
        return conditionings

    def get_stocks_df(self):
        grains_qty = self.get_rawmat_qty("g")
        hops_qty = self.get_rawmat_qty("h")
        yeasts_qty = self.get_rawmat_qty("y")
        miscs_qty = self.get_rawmat_qty("m")
        stocks_df = pd.DataFrame({"Grains": grains_qty,
                                  "Houblons": hops_qty,
                                  "Levures": yeasts_qty,
                                  "Autres": miscs_qty})
        return stocks_df

    def get_beer_df(self):
        beer_df = self.records.copy()
        if not self.records.empty:
            beer_df = beer_df.\
                merge(self.archived_recipes,
                      left_on="recipe", right_on="id").\
                loc[beer_df["conditionned"] == False,
                    ["name", "date", "volume"]]
            beer_df["date"] = beer_df["date"].apply(
                 lambda x: extract_date(x)
            )
        return beer_df

    def get_conditionned_df(self):
        conditioning_nested = self.itf.sess.get(self.itf.url +
                                                "records/conditionings").json()
        conditioning_df = pd.json_normalize(conditioning_nested,
                                            "conditioning_entry",
                                            ["records", "date"])
        if not conditioning_df.empty:
            conditioning_df = conditioning_df.\
                merge(self.records[["id", "recipe"]],
                      left_on="records",
                      right_on="id").\
                merge(self.archived_recipes[["id", "name"]],
                      left_on="recipe",
                      right_on="id").\
                loc[:, ["name", "date", "quantity", "volume"]].\
                rename(columns={"quantity": "number"}).\
                assign(date=conditioning_df["date"].
                       apply(lambda x: extract_date(x)))
        return conditioning_df

    def get_orders_df(self):
        return pd.DataFrame()

    def get_pandas_dict(self):
        pandas_dict = {
            "i": [self.get_stocks_df(), "Inventaire (kg)"],
            "f": [self.get_beer_df(), "Bière en cuve"],
            "b": [self.get_conditionned_df(), "Bière conditionnée"],
            "c": [self.get_orders_df(), "Commandes à venir"]
        }
        return pandas_dict

    def get_body(self):
        self.itf.layout["body_header"].update(Body(self.itf, "Menu principal"))

        stocks_panel = self.get_panel("i")
        self.itf.layout["body_topleft"].update(stocks_panel)

        conditionings_panel = self.get_panel("f")
        self.itf.layout["body_topright"].update(conditionings_panel)

        beer_panel = self.get_panel("b")
        self.itf.layout["body_midleft"].update(beer_panel)

        commands_panel = self.get_panel("c")
        self.itf.layout["body_midright"].update(commands_panel)
