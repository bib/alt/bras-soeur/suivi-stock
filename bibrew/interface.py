# import psycopg2
import pandas as pd
# import time
import timg
import re
import requests

from lib.rich.rich.table import Table, Column
from lib.rich.rich.layout import Layout
# from lib.rich.rich.panel import Panel
from lib.rich.rich.align import Align
from lib.rich.rich.text import Text
from lib.rich.rich.theme import Theme
from lib.rich.rich.prompt import Prompt, FloatPrompt, IntPrompt, Confirm
from lib.rich.rich.console import Console

from bibrew.base_menus import StateMachine
from bibrew.login_menu import LoginMenu
from bibrew.main_menu import MainMenu
from bibrew.rawmats_menu import (
    RawmatsMenu,
    SelectRawmat,
    RemoveRawmat,
    AddRawmat,
    UpdateRawmat
)
from bibrew.stocks_menus import (
    StocksMenu,
    StockDetailMenu,
    SelectStockRawmat,
    AddRawmatQuantity,
    RemoveStockRawmat,
    UpdateStockRawmat
)
from bibrew.recipes_menus import (
    SelectRecipe,
    CreateRecipe,
    ViewRecipe,
    RemoveRecipe,
    EditRecipe,
    UpdateRecipe,
    AddWaterInput,
    AddRecipeInput,
    RemoveRecipeInput,
    UpdateRecipeInput,
    SetParent,
    DoRecipe,
)
from bibrew.import_menus import (
    SelectImportRecipe, ImportRecipe,
    ReplaceRawmatImport, AddRawmatFromImport
)
from bibrew.records_menus import (
    RecordMenu,
    AddRecord, RemoveRecord, UpdateRecord,
    ViewRecordedRecipe,
    ConditioningMenu, SelectConditioning,
    AddConditioning, RemoveConditioning, UpdateConditioning,
    SelectConditioningEntry,
    AddConditioningEntry, RemoveConditioningEntry, UpdateConditioningEntry
)
from bibrew.orders_menu import (
    OrderMenu, ClientMenu, AddClient, RemoveClient, UpdateClient,
    AddOrder, RemoveOrder, UpdateOrder,
    ValidatedStatus, ShippedStatus, PayedStatus,
    AddBeerOrder, RemoveBeerOrder, UpdateBeerOrder,
    AddBeerToShip, RemoveBeerToShip, UpdateBeerToShip
)
from bibrew.panels import Header
from bibrew.pures import xstr

bibrew_theme = Theme(
    {
        "base": "rgb(255,102,0) on black",
        "header": "black on rgb(255,102,0)",
        "content": "white on black",
        "select": "black on wheat1",
        "sec_select": "black on light_goldenrod2",
        "select_panel": "wheat1 on black",
        "not_in_stock": "black on grey30",
        "not_enough_stock": "black on dark_red",
        "almost_enough_stock": "black on bright_red"
    }
)


class Interface:
    def __init__(self):
        self.url = None
        self.auth = (None, None)
        self.sess = requests.Session()
        self.console = Console(theme=bibrew_theme, style="content")
        self.console.prompt_style = "base"
        self.layout = Layout()
        self.header = Header(itf=self)
        self._make_layouts()
        self.menu_manager = StateMachine("master")
        self.login_menu()
        self.main_menu()
        self.rawmats_menu()
        self.stocks_menu()
        self.recipes_menu()
        self.records_menu()
        self.orders_menu()
        self.rawmats_dict = {
            "w": ["Eau", "water"],
            "g": ["Grains", "grain", "grain_stocks"],
            "h": ["Houblons", "hop", "hop_stocks"],
            "y": ["Levures", "yeast", "yeast_stocks"],
            "m": ["Autres", "misc", "misc_stocks"]
        }
        self.colnames_dict = {
            "name": "Nom",
            "quantity": "Quantité (kg)",
            "volume": "Volume (L)",
            "step": "Étape",
            "duration": "Durée",
            "form": "Forme",
            "delay": "Délai",
            "Pourcentage": "Pourcentage",
            "max_yield": "Rendement max",
            "max_proportion": "Proportion max",
            "price": "Prix",
            "aromatic": "Aromatique",
            "bitter": "Amérisant",
            "alpha": "Alpha",
            "beta": "Beta",
            "producer": "Producteur",
            "type": "Type",
            "attenuation": "Atténuation",
            "min_temp": "Température min",
            "max_temp": "Température max",
            "floculation": "Floculation",
            "date": "Date",
            "number": "Nombre",
            "initial_density": "Densité initiale",
            "final_density": "Densité finale",
            "business_name": "Raison Sociale",
            "mail": "Mail",
            "tel": "Téléphone",
            "address": "Adresse",
            "due_date": "Pour le",
            "client": "Client",
            "shipped": "Livrée",
            "payed": "Payée",
            "validated": "Validée"
        }
        self.steps_df = pd.DataFrame(
            {
                "opt": ["empat",
                        "filt",
                        "rinc",
                        "ebu",
                        "whirl",
                        "ferm",
                        "dry",
                        "cond"],
                "step": ["MASH",
                         "FILTER",
                         "RINSE",
                         "BOIL",
                         "WHIRLPOOL",
                         "FERMENTATION",
                         "DRYHOP",
                         "CONDITIONING"],
                "print": ["Empâtage",
                          "Filtration",
                          "Rinçage",
                          "Ebullition",
                          "Whirlpool",
                          "Fermentation en cuve",
                          "Dry-Hopping",
                          "Conditionnement"]
            }
        )
        self.yeast_forms = {
            "DRY": "Sèche",
            "LIQUID": "Liquide",
            "HARVEST": "Culture"
        }
        self.yeast_types = {
            "ALE": "Ale",
            "LAGER": "Lager",
            "SAISON": "Saison",
            "VOSSKVEIK": "Voss Kveik"
        }
        self.yeast_floc = {
            "LOW": "Basse",
            "MEDIUM": "Medium",
            "HIGH": "Elevée"
        }
        self.cond_types = {
            "BOTTLE_33": 0.33,
            "BOTTLE_75": 0.75,
            "KEG_20": 20,
            "KEG_30": 30
        }

    def _make_layouts(self):
        # Divide the "screen" in to three parts
        self.layout.split_column(
            Layout(name="deadspace", size=1),
            Layout(name="header", size=3),
            Layout(ratio=1, name="main"),
            Layout(size=3, name="footer")
        )
        # Divide the "main" layout in to "side" and "body"
        self.layout["main"].split_row(
            Layout(name="side"),
            Layout(name="body", ratio=3)
        )
        # Divide the "side" into "topside" and "botside"
        self.layout["side"].split_column(
            Layout(name="topside"),
            Layout(name="botside")
        )
        # Divide the "body" into four
        self.layout["body"].split_column(
            Layout(name="body_header", size=3),
            Layout(name="body_main")
        )
        self.layout["body_main"].split_row(
            Layout(name="body_left"),
            Layout(name="body_right")
        )
        self.layout["body_left"].split_column(
            Layout(name="body_topleft"),
            Layout(name="body_midleft"),
            Layout(name="body_botleft")
        )
        self.layout["body_right"].split_column(
            Layout(name="body_topright"),
            Layout(name="body_midright"),
            Layout(name="body_botright")
        )
        # Add the header and deadspace
        self.layout["deadspace"].update("")
        self.layout["header"].update(self.header)
        # Make botside and prompt invisible for the moment
        self.layout["botside"].visible = False
        self.layout["footer"].visible = False
        self.layout["body_header"].visible = False
        self.layout["body_midleft"].visible = False
        self.layout["body_botleft"].visible = False
        self.layout["body_right"].visible = False

    def print_wait_init(self):
        "Prints Acoeur logo at the beginning"
        width = self.console.size[0]
        height = self.console.size[1]*2 - 3
        obj = timg.Renderer()
        obj.load_image_from_file("Bras-Soeur_forASCII.png")
        obj.resize(width, height-1)
        obj.render(timg.ASCIIMethod)
        self.console.print(Align("Chargement en cours...",
                                 align="center"))

    def show_body_layout(self, layout_name):
        side = re.sub("body_.{3}", "", layout_name)
        self.layout[f"body_{side}"].visible = True
        self.layout[layout_name].visible = True

        o_side = "left" if side == "right" else "right"
        self.layout[f"body_{o_side}"].visible = False
        other_layouts = [x for x in [f"body_top{side}",
                                     f"body_mid{side}",
                                     f"body_bot{side}"] if x != layout_name]
        for layout in other_layouts:
            self.layout[layout].visible = False

    def one_body(self):
        """Turns only body_topleft visible"""
        self.layout["body_left"].visible = True
        self.layout["body_topleft"].visible = True
        self.layout["body_midleft"].visible = False
        self.layout["body_botleft"].visible = False
        self.layout["body_right"].visible = False

    def two_body(self):
        """Turns only body_topleft and body_topright visible"""
        self.layout["body_left"].visible = True
        self.layout["body_topleft"].visible = True
        self.layout["body_midleft"].visible = False
        self.layout["body_botleft"].visible = False
        self.layout["body_right"].visible = True
        self.layout["body_topright"].visible = True
        self.layout["body_midright"].visible = False
        self.layout["body_botright"].visible = False

    def three_body(self):
        """Turns only body_topleft, body_topright and body_midright visible"""
        self.layout["body_left"].visible = True
        self.layout["body_topleft"].visible = True
        self.layout["body_midleft"].visible = False
        self.layout["body_botleft"].visible = False
        self.layout["body_right"].visible = True
        self.layout["body_topright"].visible = True
        self.layout["body_midright"].visible = True
        self.layout["body_botright"].visible = False

    def four_body(self):
        """Turns 4 body layouts visible """
        self.layout["body_left"].visible = True
        self.layout["body_topleft"].visible = True
        self.layout["body_midleft"].visible = True
        self.layout["body_botleft"].visible = False
        self.layout["body_right"].visible = True
        self.layout["body_topright"].visible = True
        self.layout["body_midright"].visible = True
        self.layout["body_botright"].visible = False

    def five_body(self):
        """Turns 5 body layouts visible"""
        self.layout["body_left"].visible = True
        self.layout["body_topleft"].visible = True
        self.layout["body_midleft"].visible = True
        self.layout["body_botleft"].visible = True
        self.layout["body_right"].visible = True
        self.layout["body_topright"].visible = True
        self.layout["body_midright"].visible = True
        self.layout["body_botright"].visible = False

    def login_menu(self):
        """Makes login menu state"""
        login_menu = LoginMenu(name="login_menu", itf=self)
        self.menu_manager.add_state(login_menu)
        self.menu_manager.push_state("login_menu")

    def main_menu(self):
        """Makes main menu state"""
        main_menu = MainMenu(name="main_menu", itf=self)
        self.menu_manager.add_state(main_menu)

    def rawmats_menu(self):
        rawmats_menu = RawmatsMenu(name="rawmats_menu",
                                   itf=self)
        self.menu_manager.add_state(rawmats_menu)
        rawmat_menu = SelectRawmat(name="rawmat_menu",
                                   itf=self)
        rawmat_menu.add_option("a",
                               "add_rawmat",
                               "Ajouter une matière première",
                               "a")
        rawmat_menu.add_option("s",
                               "remove_rawmat",
                               "Supprimer une matière première",
                               "s",
                               select_id=True)
        rawmat_menu.add_option("m",
                               "update_rawmat",
                               "Modifier les données d'une matière première",
                               "m",
                               select_id=True)
        self.menu_manager.add_state(rawmat_menu)

        add_rawmat = AddRawmat(name="add_rawmat",
                               itf=self)
        self.menu_manager.add_state(add_rawmat)

        remove_rawmat = RemoveRawmat(name="remove_rawmat",
                                     itf=self)
        self.menu_manager.add_state(remove_rawmat)

        update_rawmat = UpdateRawmat(name="update_rawmat",
                                     itf=self)
        self.menu_manager.add_state(update_rawmat)

    def stocks_menu(self):
        """Makes stocks menu states"""
        stocks_menu = StocksMenu(name="stocks_menu", itf=self)
        self.menu_manager.add_state(stocks_menu)

        stock_details = StockDetailMenu(name="stock_details",
                                        itf=self)
        self.menu_manager.add_state(stock_details)

        select_rawmat_s = SelectRawmat(name="select_rawmat_s",
                                       itf=self)
        select_rawmat_s.add_option("\\n",
                                   "add_stock_rawmat",
                                   " \nSélectionner la ligne",
                                   "Entrée",
                                   select_id=True)
        self.menu_manager.add_state(select_rawmat_s)

        select_stock_rawmat_s = SelectStockRawmat(
            name="select_stock_rawmat_s",
            itf=self)
        select_stock_rawmat_s.add_option("\\n",
                                         "add_stock_rawmat",
                                         " \nSélectionner la ligne",
                                         "Entrée",
                                         select_id=True)
        self.menu_manager.add_state(select_stock_rawmat_s)

        add_rawmat = AddRawmatQuantity(
            name="add_stock_rawmat",
            itf=self
        )
        self.menu_manager.add_state(add_rawmat)

        remove_stock_rawmat = RemoveStockRawmat(
            name="remove_stock_rawmat",
            itf=self
        )
        self.menu_manager.add_state(remove_stock_rawmat)

        update_stock_rawmat = UpdateStockRawmat(
            name="update_stock_rawmat",
            itf=self
        )
        self.menu_manager.add_state(update_stock_rawmat)

    def recipes_menu(self):
        """Makes recipe menus states"""
        recipes_menu = SelectRecipe(name="recipes_menu",
                                    itf=self)
        recipes_menu.add_option("\\n",
                                "view_recipe",
                                "  : Voir la recette",
                                "Entrée",
                                select_id=True)
        recipes_menu.add_option("c",
                                "create_recipe",
                                "Créer une recette à partir de la sélection",
                                "c",
                                select_id=True)
        recipes_menu.add_option("m",
                                "update_recipe",
                                "Modifier la recette",
                                "m",
                                select_id=True)
        recipes_menu.add_option("e",
                                "edit_recipe",
                                "Modifier les méta-données de la recette",
                                "e",
                                select_id=True)
        recipes_menu.add_option("p",
                                "select_recipe_parent",
                                "  : Redéfinir la recette parent",
                                "p",
                                select_id=True)
        recipes_menu.add_option("s",
                                "remove_recipe",
                                "Supprimer la recette",
                                "s",
                                select_id=True)
        recipes_menu.add_option("r",
                                "do_recipe",
                                "Réaliser la recette",
                                "r",
                                select_id=True)
        recipes_menu.add_option("i",
                                "select_import_recipe",
                                "Importer une recette .xml",
                                "i")
        self.menu_manager.add_state(recipes_menu)

        select_recipe_parent = SelectRecipe(name="select_recipe_parent",
                                            itf=self)
        select_recipe_parent.add_option("\\n",
                                        "set_parent",
                                        " \nChoisir la nouvelle recette mère\n",
                                        "Entrée",
                                        select_id=True)
        self.menu_manager.add_state(select_recipe_parent)

        set_parent = SetParent(name="set_parent",
                               itf=self)
        self.menu_manager.add_state(set_parent)

        create_recipe = CreateRecipe(name="create_recipe",
                                     itf=self)
        self.menu_manager.add_state(create_recipe)

        view_recipe = ViewRecipe(name="view_recipe",
                                 itf=self)
        self.menu_manager.add_state(view_recipe)

        remove_recipe = RemoveRecipe(name="remove_recipe",
                                     itf=self)
        self.menu_manager.add_state(remove_recipe)

        edit_recipe = EditRecipe(name="edit_recipe",
                                 itf=self)
        self.menu_manager.add_state(edit_recipe)

        update_recipe = UpdateRecipe(name="update_recipe",
                                     itf=self)
        self.menu_manager.add_state(update_recipe)

        select_rawmat_r = SelectRawmat(name="select_rawmat_r",
                                       itf=self)
        select_rawmat_r.add_option("\\n",
                                   "add_recipe_input",
                                   " \nSélectionner la ligne",
                                   "Entrée",
                                   select_id=True)
        self.menu_manager.add_state(select_rawmat_r)

        select_stock_rawmat_r = SelectStockRawmat(
            name="select_stock_rawmat_r",
            itf=self
        )
        select_stock_rawmat_r.add_option("\\n",
                                         "add_recipe_input",
                                         " \nSélectionner la ligne",
                                         "Entrée",
                                         select_id=True)
        self.menu_manager.add_state(select_stock_rawmat_r)

        select_rawmat_import = SelectRawmat(name="select_rawmat_import",
                                            itf=self)
        select_rawmat_import.add_option("\\n",
                                        "replace_rawmat_import",
                                        " \nSélectionner la ligne",
                                        "Entrée",
                                        select_id=True)
        self.menu_manager.add_state(select_rawmat_import)

        add_water_input = AddWaterInput(name="add_water_input",
                                        itf=self)
        self.menu_manager.add_state(add_water_input)
        
        add_recipe_input = AddRecipeInput(name="add_recipe_input",
                                          itf=self)
        self.menu_manager.add_state(add_recipe_input)
        
        remove_recipe_input = RemoveRecipeInput(name="remove_recipe_input",
                                                itf=self)
        self.menu_manager.add_state(remove_recipe_input)
        
        update_recipe_input = UpdateRecipeInput(name="update_recipe_input",
                                                itf=self)
        self.menu_manager.add_state(update_recipe_input)
        
        do_recipe = DoRecipe(name="do_recipe",
                             itf=self)
        self.menu_manager.add_state(do_recipe)

        # Imports
        select_import_recipe = SelectImportRecipe(name="select_import_recipe",
                                                  itf=self)
        self.menu_manager.add_state(select_import_recipe)
        
        import_recipe = ImportRecipe(name="import_recipe",
                                     itf=self)
        self.menu_manager.add_state(import_recipe)
        replace_rawmat_import = ReplaceRawmatImport(
            name="replace_rawmat_import",
            itf=self
        )
        self.menu_manager.add_state(replace_rawmat_import)
        add_imported_rawmat = AddRawmatFromImport(name="add_imported_rawmat",
                                        itf=self)
        self.menu_manager.add_state(add_imported_rawmat)

    def records_menu(self):
        records = RecordMenu(name="records_menu",
                             itf=self)
        self.menu_manager.add_state(records)

        select_recipe_rec = SelectRecipe(name="select_recipe_rec",
                                         itf=self)
        select_recipe_rec.add_option("\\n",
                                     "add_record",
                                     "  : Sélectionner la recette réalisée",
                                     "Entrée",
                                     select_id=True)
        select_recipe_rec.add_option("v",
                                     "view_recipe",
                                     "Voir la recette",
                                     "v",
                                     select_id=True)
        self.menu_manager.add_state(select_recipe_rec)

        add_record = AddRecord(name="add_record",
                               itf=self)
        self.menu_manager.add_state(add_record)

        remove_record = RemoveRecord(name="remove_record",
                                     itf=self)
        self.menu_manager.add_state(remove_record)

        update_record = UpdateRecord(name="update_record",
                                     itf=self)
        self.menu_manager.add_state(update_record)

        view_recorded_recipe = ViewRecordedRecipe(name="view_recorded_recipe",
                                                  itf=self)
        self.menu_manager.add_state(view_recorded_recipe)

        conditioning_menu = ConditioningMenu(name="conditioning_menu",
                                             itf=self)
        self.menu_manager.add_state(conditioning_menu)

        add_conditioning = AddConditioning(name="add_conditioning",
                                           itf=self)
        self.menu_manager.add_state(add_conditioning)

        remove_conditioning = RemoveConditioning(name="remove_conditioning",
                                                 itf=self)
        self.menu_manager.add_state(remove_conditioning)

        update_conditioning = UpdateConditioning(name="update_conditioning",
                                                 itf=self)
        self.menu_manager.add_state(update_conditioning)

        add_conditioning_entry = AddConditioningEntry(
            name="add_conditioning_entry",
            itf=self)
        self.menu_manager.add_state(add_conditioning_entry)

        remove_conditioning_entry = RemoveConditioningEntry(
            name="remove_conditioning_entry",
            itf=self)
        self.menu_manager.add_state(remove_conditioning_entry)

        update_conditioning_entry = UpdateConditioningEntry(
            name="update_conditioning_entry",
            itf=self)
        self.menu_manager.add_state(update_conditioning_entry)

    def orders_menu(self):
        orders_menu = OrderMenu(name="orders_menu",
                                itf=self)
        self.menu_manager.add_state(orders_menu)

        select_recipe_o = SelectRecipe(name="select_recipe_o",
                                       itf=self)
        select_recipe_o.add_option("\\n",
                                   "add_beer_order",
                                   "  : Sélectionner la recette commandée",
                                   "Entrée",
                                   select_id=True)
        self.menu_manager.add_state(select_recipe_o)

        clients_menu = ClientMenu(name="clients_menu",
                                  itf=self)
        self.menu_manager.add_state(clients_menu)

        select_client_o = ClientMenu(name="select_client_o",
                                     itf=self)
        select_client_o.add_option("\\n",
                                   "add_order",
                                   "  : Sélectionner le client",
                                   "Entrée",
                                   select_id=True)
        self.menu_manager.add_state(select_client_o)

        add_client = AddClient(name="add_client",
                               itf=self)
        self.menu_manager.add_state(add_client)

        remove_client = RemoveClient(name="remove_client",
                                     itf=self)
        self.menu_manager.add_state(remove_client)

        update_client = UpdateClient(name="update_client",
                                     itf=self)
        self.menu_manager.add_state(update_client)

        add_order = AddOrder(name="add_order",
                             itf=self)
        self.menu_manager.add_state(add_order)

        remove_order = RemoveOrder(name="remove_order",
                                   itf=self)
        self.menu_manager.add_state(remove_order)

        update_order = UpdateOrder(name="update_order",
                                   itf=self)
        self.menu_manager.add_state(update_order)

        validated_status = ValidatedStatus(name="validated_status",
                                           itf=self)
        self.menu_manager.add_state(validated_status)

        shipped_status = ShippedStatus(name="shipped_status",
                                           itf=self)
        self.menu_manager.add_state(shipped_status)

        payed_status = PayedStatus(name="payed_status",
                                           itf=self)
        self.menu_manager.add_state(payed_status)

        add_beer_order = AddBeerOrder(name="add_beer_order",
                                      itf=self)
        self.menu_manager.add_state(add_beer_order)

        remove_beer_order = RemoveBeerOrder(name="remove_beer_order",
                                            itf=self)
        self.menu_manager.add_state(remove_beer_order)

        update_beer_order = UpdateBeerOrder(name="update_beer_order",
                                            itf=self)
        self.menu_manager.add_state(update_beer_order)

        add_beer_to_ship = AddBeerToShip(name="add_beer_to_ship",
                                         itf=self)
        self.menu_manager.add_state(add_beer_to_ship)

        remove_beer_to_ship = RemoveBeerToShip(name="remove_beer_to_ship",
                                               itf=self)
        self.menu_manager.add_state(remove_beer_to_ship)

        update_beer_to_ship = UpdateBeerToShip(name="update_beer_to_ship",
                                               itf=self)
        self.menu_manager.add_state(update_beer_to_ship)

        select_record_o = RecordMenu(name="select_record_o",
                                     itf=self)
        select_record_o.options = {}
        select_record_o.add_option(
            "\\n",
            "select_conditioning_o",
            "Sélectionner le brassin à associer à la commande",
            "Entrée",
            select_id=True
        )
        self.menu_manager.add_state(select_record_o)

        select_conditioning_o = SelectConditioning(name="select_conditioning_o",
                                                   itf=self)
        select_conditioning_o.add_option(
            "\\n",
            "select_conditioning_entries_o",
            "Sélectionner la date de conditionnement à associer à la commande",
            "Entrée",
            select_id=True
        )
        self.menu_manager.add_state(select_conditioning_o)

        select_conditioning_entries_o = SelectConditioningEntry(
            name="select_conditioning_entries_o",
            itf=self)
        select_conditioning_entries_o.add_option(
            "\\n",
            "add_beer_to_ship",
            "Sélectionner le volume de contenant à associer à la commande",
            "Entrée",
            select_id=True
        )
        self.menu_manager.add_state(select_conditioning_entries_o)

    def pandas_to_rich(self, pandas_df, rich_name, highlight=-1, sec_highlight=[], row_offset=0):
        """Converts pandas dataframe to rich_table"""
        raw_colnames = list(pandas_df.columns)
        if raw_colnames == [] or pandas_df.shape[0] == 0:
            return Text(f"{rich_name} \n Rien à afficher",
                        justify="center",
                        style=self.console.get_style("content"))
        colnames = map(lambda x: (Column(self.colnames_dict[x], no_wrap=True)
                                  if x in self.colnames_dict
                                  else Column(x, no_wrap=True)),
                       raw_colnames)
        rich_table = Table(title=rich_name, *colnames)
        i = 0+row_offset
        for index, row in pandas_df.iterrows():
            rich_row = []
            for colname in raw_colnames:
                if i == highlight:
                    rich_row.append(
                        Text(xstr(row[colname]),
                             style=self.console.get_style("select"))
                    )
                else:
                    if i in sec_highlight:
                        rich_row.append(
                            Text(xstr(row[colname]),
                                 style=self.console.get_style("sec_select"))
                        )
                    else:
                        rich_row.append(
                            Text(xstr(row[colname]),
                                 style=self.console.get_style("content"))
                        )
            rich_table.add_row(*rich_row)
            i += 1
        return rich_table

    def ask_int(self, *args, **kwargs):
        """Defines console and layouts in IntPrompt.ask, """
        res = IntPrompt.ask(console=self.console,
                            layout=self.layout,
                            layout_name="footer",
                            *args, **kwargs)
        return res

    def ask_float(self, *args, **kwargs):
        """Defines console and layouts in FloatPrompt.ask"""
        res = FloatPrompt.ask(console=self.console,
                              layout=self.layout,
                              layout_name="footer",
                              *args, **kwargs)
        return res

    def ask_str(self, *args, **kwargs):
        """Defines console and layouts in Prompt.ask"""
        res = Prompt.ask(console=self.console,
                         layout=self.layout,
                         layout_name="footer",
                         *args, **kwargs)
        return res

    def ask_confirm(self, *args, **kwargs):
        """Defines console and layouts in Confirm.ask"""
        res = Confirm.ask(console=self.console,
                          layout=self.layout,
                          layout_name="footer",
                          *args, **kwargs)
        return res

    def format_duration(self, pandas_df):
        """Formats pandas_df duration column with unit"""
        pandas_df = pandas_df.copy()
        pandas_df["duration"] = pandas_df["duration"].apply(
            lambda x: str(round(x)))
        pandas_df.loc[pandas_df.step.isin(["MASH",
                                           "FILTER",
                                           "RINSE",
                                           "BOIL",
                                           "WHIRLPOOL"]),
                      'duration'] = pandas_df.duration + "'"
        pandas_df.loc[pandas_df.step.isin(["FERMENTATION",
                                           "DRYHOP",
                                           "CONDITIONING"]),
                      'duration'] = pandas_df.duration + "j"
        return pandas_df


class MyCompleter():  # Custom completer
    def __init__(self, options):
        self.options = sorted(options)

    def complete(self, text, state):
        """autocompletion"""
        if state == 0:  # on first trigger, build possible matches
            if text:  # cache matches (entries that start with entered text)
                self.matches = [
                    s for s in self.options if s and s.startswith(text)]
            else:  # no text entered, all matches possible
                self.matches = self.options[:]

        # return match indexed by state
        try:
            return self.matches[state]
        except IndexError:
            return None


def alcool_prompt():
    d_deb = input("Densité initiale : ")
    d_fin = input("Densité finale : ")
    sucre_emb = input("Quantité de sucre à l'embouteillage (g/L) : ")
    try:
        d_deb = float(d_deb)
        d_fin = float(d_fin)
        sucre_emb = float(sucre_emb)
    except TypeError:
        return "wrong_entry"
    if d_deb > 2:
        d_deb = d_deb / 1000
    if d_fin > 2:
        d_fin = d_fin / 1000
    return (d_deb, d_fin, sucre_emb)


def dens_temp_prompt():
    dens = input("Densité lue : ")
    temp = input("Température : ")
    try:
        dens = float(dens)
        temp = float(temp)
    except TypeError:
        return "wrong_entry"
    if dens < 2:
        dens = dens * 1000
    return (dens, temp)


def dens_to_plato_prompt():
    dens = input("Densité : ")
    dens = dens.replace(",", ".")
    try:
        dens = float(dens)
    except ValueError:
        return "NA"
    if dens > 2:
        dens = dens / 1000
    plato = (
        (-1 * 616.868)
        + (1111.14 * dens)
        - (630.272 * dens ** 2)
        + (135.997 * dens ** 3)
    )
    return plato


def pitch_rate_prompt():
    dens = input("Densité du moût : ")
    vol = input("Volume de moût : ")
    beer_type = input("Type de bière : (A)le ou (L)ager : ")
    try:
        dens = float(dens)
        vol = float(vol)
    except TypeError:
        return "wrong_entry"
    if dens > 2:
        dens = dens / 1000
    return (vol, dens, beer_type)

