"""Setup"""
from setuptools import setup

setup(
   name='bibrew',
   version='0.9.0',
   author='Dr Loiseau',
   author_email='dr.loiseau@lebib.org',
   packages=['bibrew', 'lib'],
   scripts=[],
   url='',
   license='LICENSE.txt',
   description='Client for bibrew : brew helper',
   long_description='',
   install_requires=[
       "pandas",
       "logbook",
       "unidecode",
       "requests",
       "datetime",
       "lxml",
       "timg",
       "curtsies",
       "babel"
   ],
)
