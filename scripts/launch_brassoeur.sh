#!/bin/bash

cd $(dirname $0) && pwd
cd ..
xfce4-terminal --maximize -e "python3 -m bibrew" || gnome-terminal --maximize -- "python3 -m bibrew" || mate-terminal --maximize -e "python3 -m bibrew" || echo "term emulator not found"
